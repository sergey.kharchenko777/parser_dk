<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/products', [ProductController::class, 'index'])->name('all.products');
Route::get('/product/{product}', [ProductController::class, 'show'])->name('show.product');

Route::get('/update-prods-and-cats', [ProductController::class, 'importProductsAndCategories'])
    ->name('update.prods.and.cats');

Route::get('/update-prod-price', [ProductController::class, 'updatePrices'])->name('update.prod.price');



