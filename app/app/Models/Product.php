<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = ['id', 'name', 'description', 'price', 'code', 'category_id'];

//    /**
//     * @var string[]
//     */
//    protected $with = ['prices'];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function prices(): HasMany
    {
        return $this->hasMany(Price::class);
    }

    public function stores(): BelongsToMany
    {
        return $this->belongsToMany(Store::class, 'prices')->withPivot('price');
    }
}
