<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Services\PriceImporter;
use App\Services\ProductImporter;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class ProductController extends Controller
{
    public function index(): View
    {
        $products = Product::with('category')->get();

        return view('product.list', ['products' => $products]);
    }

    public function show(Product $product): View
    {
        return view('product.detail', ['product' => $product->load('stores')]);
    }

    public function importProductsAndCategories(ProductImporter $productImporter): View
    {
        $productImporter->import();
        Session::flash('success', 'Продукти та категоії успішно оновленно!');

        return view('product.import');
    }

    public function updatePrices(PriceImporter $priceImporter): View
    {
        $priceImporter->import();
        Session::flash('success', 'Ціни на продукти успішно оновленно!');

        return view('product.import');
    }
}
