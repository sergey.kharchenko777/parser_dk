<?php

namespace App\Console\Commands;

use App\Services\ProductImporter;
use Illuminate\Console\Command;

class ProductImporterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Команда оновлює продукти та категорії в БД';
    /**
     * @var ProductImporter
     */
    private $productImporter;

    /**
     * @param ProductImporter $productImporter
     */
    public function __construct(ProductImporter $productImporter)
    {
        parent::__construct();
        $this->productImporter = $productImporter;
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->productImporter->import();
        $this->info('Продукти та категоії успішно оновленно!');
    }
}
