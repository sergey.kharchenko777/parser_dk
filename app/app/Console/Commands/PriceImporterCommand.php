<?php

namespace App\Console\Commands;

use App\Services\PriceImporter;
use Illuminate\Console\Command;

class PriceImporterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import-prices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Команда оновлює ціни на продукти в БД';

    /**
     * @var PriceImporter
     */
    private $priceImporter;

    /**
     * @param PriceImporter $priceImporter
     */
    public function __construct(PriceImporter $priceImporter)
    {
        parent::__construct();
        $this->priceImporter = $priceImporter;
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->priceImporter->import();
        $this->info('Ціни на продукти успішно оновленно!');
    }
}
