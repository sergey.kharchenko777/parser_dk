<?php

namespace App\Providers;

use App\Interfaces\PriceProviderInterface;
use App\Interfaces\ProductProviderInterface;
use App\Services\JsonPriceProvider;
use App\Services\JsonProductProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Model;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(ProductProviderInterface::class, JsonProductProvider::class);
        $this->app->bind(PriceProviderInterface::class, JsonPriceProvider::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Model::preventLazyLoading(! app()->isProduction());
    }
}
