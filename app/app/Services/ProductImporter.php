<?php

namespace App\Services;

use App\Interfaces\ProductProviderInterface;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Log;

class ProductImporter
{
    /**
     * @var ProductProviderInterface
     */
    private $productProvider;

    /**
     * Array of existing categories
     * @var array
     */
    private $categories = [];

    /**
     * @param ProductProviderInterface $productProvider
     */
    public function __construct(ProductProviderInterface $productProvider)
    {
        $this->productProvider = $productProvider;
    }

    /**
     * Update categories and products
     */
    public function import()
    {
        foreach ($this->productProvider->getProducts() as $item) {
            $this->processProduct($item);
        }
    }

    public function processProduct(array $item)
    {
        $product = Product::firstWhere('id', $item['id']);
        if ($product) {
            $product->name = $item['name'];
            $product->description = $item['description'];
            $product->price = $item['price'];
            $product->code = $item['code'];
        }
        else
        {
            $product = new Product([
                'id'          => $item['id'],
                'name'        => $item['name'],
                'description' => $item['description'],
                'price'       => $item['price'],
                'code'        => $item['code']
            ]);
        }
        $product->category()->associate($this->processCategory($item['category']));
        $product->save();
    }

    /**
     * Data of the category
     * @param array $category
     */
    private function processCategory(array $category)
    {
        if (key_exists($category['id'], $this->categories)) {
            $cat = $this->categories[$category['id']];
        }
        else
        {
            $cat = Category::firstWhere('id', $category['id']);
            if ($cat) {
                $this->categories[$category['id']] = $cat;
            }
        }
        if ($cat) {
            $cat->name = $category['name'];
        }
        else
        {
            $cat = new Category([
                'id'    => $category['id'],
                'name'  => $category['name']
            ]);
        }
        $cat->save();

        return $cat;
    }
}
