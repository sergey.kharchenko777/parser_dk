<?php

namespace App\Services;

use App\Interfaces\PriceProviderInterface;
use App\Models\Price;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;


class PriceImporter
{
    /**
     * The array existing ids products and categories
     * @var array
     */
    private $prices = [];

    /**
     * @var PriceProviderInterface
     */
    private $priceProvider;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param PriceProviderInterface $priceProvider
     * @param LoggerInterface $logger
     */
    public function __construct(
        PriceProviderInterface $priceProvider,
        LoggerInterface $logger
    )
    {
        $this->logger = Log::channel('product');
        $this->priceProvider = $priceProvider;
    }

    public function import()
    {
        if (!$prices = $this->priceProvider->getPrices()) {
            return;
        }

        $this->init();

        foreach ($prices as $item) {
            $itemExists = true;
            if (!in_array($item['product_id'], $this->prices['products'])) {
                $itemExists = false;
                $this->logger->info("Продукту з id: {$item['product_id']} неіснує!");
            }
            if (!in_array($item['store_id'], $this->prices['stores'])) {
                $itemExists = false;
                $this->logger->info("Магазину з id: {$item['store_id']} неіснує!");
            }
            if ($itemExists == false) {
                continue;
            }
            $price = Price::where('product_id', $item['product_id'])
                ->where('store_id', $item['store_id'])
                ->first();
            if (!$price) {
                $price = new Price([
                    'price'         => $item['price'],
                    'product_id'    => $item['product_id'],
                    'store_id'      => $item['store_id']
                    ]);
            }
            else
            {
                $price->price = $item['price'];
            }
            $price->save();
        }
    }

    /**
     * Create array existing prods & cats in this->prices
     */
    private function init()
    {
        Product::all('id')->each(fn($product) => $this->prices['products'][] = $product->id);
        Store::all('id')->each(fn($store) => $this->prices['stores'][] = $store->id);
    }
}
