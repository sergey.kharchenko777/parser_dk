<?php

namespace App\Services;

use App\Interfaces\PriceProviderInterface;
use Illuminate\Support\Facades\Storage;

class JsonPriceProvider implements PriceProviderInterface
{
    public function getPrices()
    {
        $prices = json_decode(Storage::get('files/price.json'), true);

        return $prices;
    }
}
