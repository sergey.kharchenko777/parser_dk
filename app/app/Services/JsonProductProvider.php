<?php

namespace App\Services;

use App\Interfaces\ProductProviderInterface;
use Illuminate\Support\Facades\Storage;

class JsonProductProvider implements ProductProviderInterface
{
    public function getProducts()
    {
        $products = json_decode(Storage::get('files/product.json'), true);

        return $products;
    }
}
