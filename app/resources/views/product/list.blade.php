<x-layout>
    <div class="container">
        <h1 class="mt-3 mb-4">Products</h1>
        <a href="{{ route('update.prods.and.cats') }}" class="btn btn-success mt-3 mb-4">Оновити продукти та категорії</a>
        <a href="{{ route('update.prod.price') }}" class="btn btn-danger mt-3 mb-4">Оновити ціну на продукти</a>
        <div class="row">
            @foreach($products as $product)
            <div class="col-md-4">
                <div class="card mb-4">
                    <img src="https://via.placeholder.com/200x150" alt="{{ $product->name }}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">{{ $product->name }}</h5>
                        <p class="card-text">{{ $product->description }}</p>
                        <span>Категорія:</span>
                        <h6>{{ $product->category->name }}</h6>
                        <div class="d-flex justify-content-between align-item-center">
                            <a href="{{ route('show.product', $product->id) }}" class="btn btn-dark">Детальніше</a>
                            <span class="h5 mt-auto">{{ $product->price }} €</span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</x-layout>
