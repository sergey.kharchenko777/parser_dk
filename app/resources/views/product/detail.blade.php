<x-layout>
    <div class="container">
        <div class="row mt-3 mb-4">
            <div class="col-md-4">
                <img src="https://via.placeholder.com/600x400" alt="{{ $product->name }}" class="img-fluid">
            </div>
            <div class="col-md-8">
                <h1 class="mt-4 mt-md-0">{{ $product->name }}</h1>
                <h2>{{ $product->price }} €</h2>
                <hr>
                <b>Description: </b>{{ $product->description }}
        </div>
        <div>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Код</th>
                    <th scope="col">Продукт</th>
                    <th scope="col">Категорія</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">{{ $product->code }}</th>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->category->name }}</td>
                </tr>
                </tbody>
            </table>
        </div>
            @if($product->stores)
                <div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Магазин</th>
                            <th scope="col">Ціна продукта в магазині</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($product->stores as $store)
                            <tr>
                                <th scope="row">{{ $store->address }}</th>
                                <td>{{ $store->pivot->price }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
</x-layout>
