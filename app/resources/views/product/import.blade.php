<x-layout>
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {{ $message }}
                    </div>
                @endif
                <a href="{{ route('all.products') }}">Повернутись до списку товарів</a>
            </div>
        </div>
    </div>
</x-layout>
