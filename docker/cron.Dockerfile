FROM php:8.1.0-fpm-alpine

RUN docker-php-ext-install pdo pdo_mysql

RUN chown www-data: /var/www/html
COPY --chown=www-data: ./app /var/www/html

#COPY ./volumes/php/conf.d/crontab /etc/crontabs/root
ADD ./volumes/cron/cron.d/crontab /etc/cron.d/crontab
RUN chown www-data: /etc/cron.d/crontab
RUN crontab -u www-data /etc/cron.d/crontab

CMD ["crond", "-f"]